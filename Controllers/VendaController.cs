using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        [HttpPost]
        public IActionResult Registrar(Venda venda){

            Banco.context.Add(venda);

            //verificação se há pelo menos 1 item na venda
            if(venda.Itens.Count > 0){

                VendaViewDTO vendaTotal = new VendaViewDTO(venda); //transformação da classe Venda para classe VendaViewDTO, para visualização completa da venda

                return Ok(vendaTotal);
            }

            return BadRequest("A Venda deve possuir pelo menos 1 item.");
        }

        [HttpGet("{id}")]
        public IActionResult ConsultarPorId(int id){
            
            var vendaBanco = Banco.context.Find(x => x.Id == id); //encontrar a venda pelo ID

            //verifica se foi encontrado
            if(vendaBanco == null)
                return NotFound();

            VendaViewDTO venda = new VendaViewDTO(vendaBanco);

            return Ok(venda);
        }

        [HttpPut("{id}/{status}")]
        public IActionResult Atualizar(int id, string status){

            var vendaBanco = Banco.context.Find(x => x.Id == id);   

            if(vendaBanco == null)
                return NotFound();

            //verificação de Transição de Status, tentei criar um metódo externo mas não tive ideia em como implementar da melhor maneira
            //se era melhor criar uma classe só pra verificações ou colocá-lo em uma classe já existente.
            if((vendaBanco.Status == "Aguardando Pagamento" && status == "Pagamento Aprovado") ||
               (vendaBanco.Status == "Aguardando Pagamento" && status == "Cancelada") ||
               (vendaBanco.Status == "Pagamento Aprovado" && status == "Enviado para Transportadora") ||
               (vendaBanco.Status == "Pagamento Aprovado" && status == "Cancelada") ||
               (vendaBanco.Status == "Enviado para Transportadora" && status == "Entregue")){

                vendaBanco.Status = status;

                VendaViewDTO venda = new VendaViewDTO(vendaBanco);

                return Ok(venda);
            }
            
            return BadRequest("Atualização de Status inválida.");
        }
    }
}