using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    /// <summary>
    /// Classe estática que armazena uma lista estática de Vendas
    /// </summary>
    public static class Banco
    {
        /// <summary>
        /// Representa uma lista estática de Vendas
        /// </summary>
        public static List<Venda> context;
        /// <summary>
        /// Representa um contador que é incrementado um a um a cada vez que é utilizado
        /// </summary>
        public static int contador;

        static Banco(){
            
            contador = 0;
            context = new List<Venda>();
        }
    }
}