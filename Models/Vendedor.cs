using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    /// <summary>
    /// Classe que representa os dados de um Vendedor
    /// </summary>
    public class Vendedor
    {
        /// <summary>
        /// Representa o CPF de um Vendedor
        /// </summary>
        public string Cpf { get; set; }

        /// <summary>
        /// Representa o Nome de um Vendedor
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// Representa o Email de um Vendedor
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Representa o Telefone de um Vendedor
        /// </summary>
        public string Telefone { get; set; }
    }
}