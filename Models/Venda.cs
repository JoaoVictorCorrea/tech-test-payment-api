using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    /// <summary>
    /// Classe que representa a classe Venda
    /// </summary>
    public class Venda
    {
        public Venda(){

            //Id, Data e Status são inseridos pelo próprio sistema

            this.Id = Banco.contador;
            this.Data = DateTime.Now;
            this.Status = "Aguardando Pagamento";

            Banco.contador++;
        }

        /// <summary>
        /// Representa o ID de uma venda
        /// </summary>
        [JsonIgnore] // foi utilizado 'JsonIgnore' para a propriedade ser ignorada na hora de ser inserida no Json na API
        public int Id { get; set; }

        /// <summary>
        /// Representa os dados do vendedor que realizou a venda
        /// </summary>
        public Vendedor DadosVendedor { get; set; }

        /// <summary>
        /// Representa a Data em que a venda fui efetivada
        /// </summary>
        [JsonIgnore]
        public DateTime Data { get; set; }

        /// <summary>
        /// Representa os Itens que foram vendidos na venda
        /// </summary>
        public List<Item> Itens { get; set; }

        /// <summary>
        /// Representa o atual Status da venda
        /// </summary>
        [JsonIgnore]
        public string Status { get; set; } //Tentei fazer o Status em formato de Enum porém pesquisando e estudando percebi que é
                                           //complicado fazer um enum de Strings ainda que eu precisava ter um espaço entre as palavras
                                           //como: 'Enviado para Transportadora'
    }
}