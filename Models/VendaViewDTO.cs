using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    /// <summary>
    /// Classe que representa a visualização completa da classe Venda
    /// </summary>
    public class VendaViewDTO
    {
        public VendaViewDTO(Venda venda){

            this.Id = venda.Id;
            this.DadosVendedor = venda.DadosVendedor;
            this.Data = venda.Data;
            this.Itens = venda.Itens;
            this.Status = venda.Status;
        }

        /// <summary>
        /// Representa o ID de uma venda
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Representa os dados do vendedor que realizou a venda
        /// </summary>
        public Vendedor DadosVendedor { get; set; }

        /// <summary>
        /// Representa a Data em que a venda fui efetivada
        /// </summary>
        public DateTime Data { get; set; }

        /// <summary>
        /// Representa os Itens que foram vendidos na venda
        /// </summary>
        public List<Item> Itens { get; set; }

        /// <summary>
        /// Representa o atual Status da venda
        /// </summary>
        public string Status { get; set; }
    }
}