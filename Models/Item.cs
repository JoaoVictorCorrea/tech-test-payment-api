using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    /// <summary>
    /// Classe que representa um Item de uma venda
    /// </summary>
    public class Item
    {
        /// <summary>
        /// Representa a Descrição de um item
        /// </summary>
        public string Descricao { get; set; }

        /// <summary>
        /// Representa o Preço de um item
        /// </summary>
        public decimal Preco { get; set; }

        /// <summary>
        /// Representa a Quantidade que foi comprada do item
        /// </summary>
        public int Quantidade { get; set; }
    }
}